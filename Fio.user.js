// ==UserScript==
// @name         Fio
// @namespace    http://tampermonkey.net/
// @version      0.2.1
// @description  try to take over the world!
// @author       You
// @match        https://www.fio.cz/e-broker/e-portfolio.cgi?menu=0
// @icon         https://www.google.com/s2/favicons?domain=fio.cz
// @grant        GM.xmlHttpRequest
// @connect      127.0.0.1
// ==/UserScript==

(function() {
    'use strict';

    const output = "python b.py uu \"fio-broker\" " + $('#portfolio_table  tr:last')[0].children[7].innerHTML.replace(',', '.').replace(/[\sCZK]/g, '');
    console.log(output);

    GM.xmlHttpRequest({
        method: "POST",
        url: "http://127.0.0.1:8000/uu/",
        data: JSON.stringify([output]),
        headers: {
            "Content-Type": "application/json"
        },
        onload: function(response) {
            //if (response.responseText.indexOf("Logged in as") > -1) {
            //  location.href = "http://www.example.net/dashboard";
            //}
        }
    });

})();

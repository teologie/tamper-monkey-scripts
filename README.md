# Skpript na získání ID lekce na Neolms

Skript alternuje název lekce přidáním ID na konci v závorkách. Zároveň toto ID automaticky **kopíruje** do systémové schránky, stačí tedy pouze potom udělat CTRL+V.

*Pozn.:*
 * *V případě, že se ID nezobrazuje proveďte znovunačtení stránky pomocí F5.*
 * *!!! indikují, že ve zdrojovém kódu stránky se vyskytuje více ID a vypisuje se poslední.*

![screenshot](/img/screenshot.png)

## Postup instalace

* nainstalujte Tampermonkey
  * Chrome https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=cs
  * FF https://addons.mozilla.org/cs/firefox/addon/tampermonkey/
* instalujte skript do Tampermonkey otevřením adresy https://gitlab.com/teologie/tamper-monkey-scripts/-/raw/master/Neolms.user.js


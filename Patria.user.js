// ==UserScript==
// @name         Patria
// @namespace    http://tampermonkey.net/
// @version      0.2.1
// @description  try to take over the world!
// @author       You
// @match        https://webtrader.patria.cz/
// @icon         https://www.google.com/s2/favicons?domain=patria.cz
// @grant        GM.xmlHttpRequest
// @connect      127.0.0.1
// ==/UserScript==

(function() {
    'use strict';
    const lines = [2, 3, 5];
    var output = '';
    var list = []

    lines.forEach(printline);
    console.log(output);

    function printline(value) {
        const item = "python b.py uu \"Patria-" + document.querySelector("#portfolioSummary > div > p:nth-child(" + value + ") > span:nth-child(1)").innerText.replace(' ', '-') + "\" " + document.querySelector("#portfolioSummary > div > p:nth-child(" + value + ") > span:nth-child(2)").innerText.replace(/[\sCZK]/g, '')
        output += item + "\n";
        list.push(item);
    }

    GM.xmlHttpRequest({
        method: "POST",
        url: "http://127.0.0.1:8000/uu/",
        data: JSON.stringify(list),
        headers: {
            "Content-Type": "application/json"
        },
        onload: function(response) {
            //if (response.responseText.indexOf("Logged in as") > -1) {
            //  location.href = "http://www.example.net/dashboard";
            //}
        }
    });


})();

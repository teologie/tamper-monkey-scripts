// ==UserScript==
// @name         Mintos
// @namespace    http://tampermonkey.net/
// @version      0.2.2
// @description  try to take over the world!
// @author       You
// @match        https://www.mintos.com/cs/prehled-uctu/
// @icon         https://www.google.com/s2/favicons?domain=mintos.com
// @grant        GM.xmlHttpRequest
// @connect      127.0.0.1
// ==/UserScript==

(function() {
    'use strict';
    setTimeout(() => {
        const output = "python b.py uu \"Mintos\" " + document.querySelector("#main-sub-wrapper > div.content.app > main > main > div.m-u-color-n2.m-u-padding-bottom-7.m-u-padding-top-6 > div > div.m-o-grid > div:nth-child(1) > div > h2").innerText.replace(/[\s€]/g, '');
        console.log(output);

        GM.xmlHttpRequest({
            method: "POST",
            url: "http://127.0.0.1:8000/uu/",
            data: JSON.stringify([output]),
            headers: {
                "Content-Type": "application/json"
            },
            onload: function(response) {
                //if (response.responseText.indexOf("Logged in as") > -1) {
                //  location.href = "http://www.example.net/dashboard";
                //}
            }
        });

    }, 5000);

})();

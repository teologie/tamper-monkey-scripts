// ==UserScript==
// @name         Trinity
// @namespace    http://tampermonkey.net/
// @version      0.2.1
// @description  try to take over the world!
// @author       You
// @match        https://ib.trinitybank.cz/prehled
// @icon         https://www.google.com/s2/favicons?domain=trinitybank.cz
// @grant        GM.xmlHttpRequest
// @connect      127.0.0.1
// ==/UserScript==

(function() {
    'use strict';

    const a = document.querySelector("#main_VKL > div");
    console.dir(a);

    var output = '';
    var list = []
    for (var i = 0; i < a.children.length; i++) {
        const item = "python b.py uu \"" + a.children[i].firstElementChild.children[0].children[1].innerText + "\" " + a.children[i].firstElementChild.children[3].children[0].innerText.replace(',', '.').replace(/[\sCZK]/g, '');
        output += item + "\n";
        list.push(item);
    }
    console.log(output);

    GM.xmlHttpRequest({
        method: "POST",
        url: "http://127.0.0.1:8000/uu/",
        data: JSON.stringify(list),
        headers: {
            "Content-Type": "application/json"
        },
        onload: function(response) {
            //if (response.responseText.indexOf("Logged in as") > -1) {
            //  location.href = "http://www.example.net/dashboard";
            //}
        }
    });


})();
